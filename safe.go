package send2statsd

import (
	"time"
)

type noStatsd struct{}

func (n noStatsd) Counter(float32, string, ...int)          {}
func (n noStatsd) Timing(float32, string, ...time.Duration) {}
func (n noStatsd) Gauge(float32, string, ...string)         {}

// Noop returns a struct that satisfies the Statter interface but silently
// ignores all Statter method invocations. It's designed to be used when normal
// send2statsd construction fails, eg.
//
//    s, err := send2statsd.Dial("udp", someEndpoint)
//    if err != nil {
//        log.Printf("not sending statistics to statsd (%s)", err)
//        s = send2statsd.Noop()
//    }
//
func Noop() Statter {
	return noStatsd{}
}
