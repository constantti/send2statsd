# send2statsd

Get to Statsd: forward simple statistics to a statsd server.

[![Build Status][1]][2] [![GoDoc][3]][4]

[1]: https://secure.travis-ci.org/peterbourgon/send2statsd.png
[2]: http://www.travis-ci.org/peterbourgon/send2statsd
[3]: https://godoc.org/github.com/peterbourgon/send2statsd?status.svg
[4]: https://godoc.org/github.com/peterbourgon/send2statsd

# Usage

send2statsd provides a Statsd object, which provides some convenience functions for
each of the supported statsd statistic-types. Just call the relevant function
on the Statsd object wherever it makes sense in your code.

```go
s, err := send2statsd.Dial("udp", "statsd-server:8125")
if err != nil {
	// do something
}

s.Counter(1.0, "my.silly.counter", 1)
s.Timing(1.0, "my.silly.slow-process", time.Since(somethingBegan))
s.Timing(0.2, "my.silly.fast-process", 7*time.Millisecond)
s.Gauge(1.0, "my.silly.status", "green")
```

If you need to add a prefix to your metrics (for example, if you need to use an 
API key to send metrics to your statsd service) just use `DialWithPrefix` instead
when instantiating your statsd struct:

```go
s, err := send2statsd.DialWithPrefix("udp", "statsd-server:8125", "my-cool-prefix")
if err != nil {
	// do something
}

s.Counter(1.0, "this.gets.prefixed", 1)
```

If you use a standard UDP connection to a statsd server, all 'update'-class
functions are goroutine safe. They should return quickly, but they're safe to
fire in a seperate goroutine.

